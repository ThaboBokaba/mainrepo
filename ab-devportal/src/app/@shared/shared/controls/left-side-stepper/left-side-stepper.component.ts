import { Component, OnInit, Input } from '@angular/core';
import { LeftSideStepper } from '../../models/stepper/left-side-stepper';

@Component({
  selector: 'app-left-side-stepper',
  templateUrl: './left-side-stepper.component.html',
  styleUrls: []
})
export class LeftSideStepperComponent implements OnInit {

  @Input()
  public items: LeftSideStepper[];

  constructor() { }

  ngOnInit(): void {
    if (!this.items) {
      console.warn('Stepper does not have any items in it');
    }
  }

}
