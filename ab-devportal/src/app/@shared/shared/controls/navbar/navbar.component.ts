import { Component, OnInit, Input } from '@angular/core';
import { IMenu } from '../../models/menu/imenu';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: []
})
export class NavbarComponent implements OnInit {

  @Input()
  public menuItems: IMenu[];

  constructor() { }

  ngOnInit(): void {
    if (!this.menuItems) {
      console.warn('No menu items provided as input to the menu component');
    }
  }

}
