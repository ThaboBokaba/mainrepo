import { IApplicationModel } from '../iapplication-model';

export interface IMenu extends IApplicationModel {
    name: string;
    link: string;
    data: any[];
    isCurrent: boolean;
    isDisabled: boolean;
}
