export interface LeftSideStepper {
    index: number;
    name: string;
    image: string;
    validated: boolean; //will reference whether the stepper items has passed validation for next item
    next: LeftSideStepper; //point to next element
    prev: LeftSideStepper; //point to prev... if prev is null, then stepper is first item
}
