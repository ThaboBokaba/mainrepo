import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';
import { NavbarComponent } from './controls/navbar/navbar.component';
import { MenuService } from './services/menu-service';
import { LeftSideStepperComponent } from './controls/left-side-stepper/left-side-stepper.component';
import { LeftSideStepperService } from './services/left-side-stepper-service';
import { RightSideContentComponent } from './controls/right-side-content/right-side-content.component';



@NgModule({
  declarations: [NavbarComponent, LeftSideStepperComponent, RightSideContentComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [MenuService, LeftSideStepperService],
  exports: [NavbarComponent, LeftSideStepperComponent, RightSideContentComponent]
})
export class SharedModule { }
