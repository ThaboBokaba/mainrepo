import { Injectable } from '@angular/core';
import { LeftSideStepper } from '../models/stepper/left-side-stepper';

@Injectable()
export class LeftSideStepperService {
    constructor() {}

    /**
     * load all steppers for the given page
     * 
     * @param pageId 
     */
    public getPageSteppers(pageId: string): LeftSideStepper[] {
        return null;
    }
}
