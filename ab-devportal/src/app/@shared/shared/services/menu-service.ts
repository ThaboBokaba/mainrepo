import { Injectable } from '@angular/core';
import { IMenu } from '../models/menu/imenu';

@Injectable()
export class MenuService {
    constructor() {}

    public getMenu(): IMenu[] {
        const homeMenuItem: IMenu = {
            name : 'Home',
            link : '/public/home',
            data: null,
            isCurrent: false,
            isDisabled: false
        };

        const productMenuItem: IMenu = {
            name : 'API\'s',
            link : '/incognito/products',
            data: null,
            isCurrent: false,
            isDisabled: false
        };

        const aboutMenuItem: IMenu = {
            name : 'About',
            link : '/public/about',
            data: null,
            isCurrent: false,
            isDisabled: false
        };

        const menuItems: IMenu[] = [];
        menuItems.push(homeMenuItem);
        menuItems.push(productMenuItem);
        menuItems.push(aboutMenuItem);

        return menuItems;
    }
}
