import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContainerComponent } from './components/container/container.component';
import { AboutComponent } from './components/about/about.component';


const routes: Routes = [
  {
    path: '',
    component: ContainerComponent,
    data: {
        menu: 'action'
    },
    children: [
        {
            path: '',
            component: HomeComponent,
            redirectTo: 'home'
        },
        {
          path: 'home',
          component: HomeComponent
        },
        {
          path: 'about',
          component: AboutComponent
        },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
