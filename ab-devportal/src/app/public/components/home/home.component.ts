import { Component, OnInit } from '@angular/core';
import { LeftSideStepperService } from 'src/app/@shared/shared/services/left-side-stepper-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent implements OnInit {

  constructor(private leftSideStepperService: LeftSideStepperService) { }

  ngOnInit(): void {
  }

}
