import { Component, OnInit } from '@angular/core';
import { IMenu } from 'src/app/@shared/shared/models/menu/imenu';
import { MenuService } from 'src/app/@shared/shared/services/menu-service';
import { LeftSideStepperService } from 'src/app/@shared/shared/services/left-side-stepper-service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: []
})
export class ContainerComponent implements OnInit {

  currentMenu = 'Welcome to our Developer portal';
  public menuItems: IMenu[] = [];

  constructor(private menuService: MenuService, private leftSideStepperService: LeftSideStepperService) { }

  ngOnInit(): void {
    this.menuItems = this.menuService.getMenu();
    // console.log(this.menuItems);
  }

  public onactivate(event): void {
    console.log('Router activated ');
    console.log(event);
  }

}
