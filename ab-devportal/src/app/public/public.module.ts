import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '../@shared/shared/shared.module';
import { ContainerComponent } from './components/container/container.component';

@NgModule({
  declarations: [HomeComponent, ContainerComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    SharedModule
  ]
})
export class PublicModule { }
