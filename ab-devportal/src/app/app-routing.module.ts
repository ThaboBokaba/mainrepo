import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerComponent } from './public/components/container/container.component';
import { HomeComponent } from './public/components/home/home.component';


const routes: Routes = [
  { path: 'public', loadChildren: () => import('./public/public.module').then(m => m.PublicModule) },

  {
    path: '**',
    component: ContainerComponent,
    data: {
        menu: 'action'
    },
    children: [
        {
            path: '',
            // component: NotFoundComponent
            component: HomeComponent
        }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
