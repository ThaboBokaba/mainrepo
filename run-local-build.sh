#!/bin/bash

containerId=$1
imageId=$2

if [ "${#containerId}" == 0 ] || [ "${#imageId}" == 0 ]
    then
        echo "Please enter container and image id in order to deploy changes to ngnix... i.e. sh run-local-build.sh containerName imageName"
        exit
fi

cd ab-devportal
echo "Building Angular Application ..."
ng build --prod
cd ..
echo "Angular Application successfully built ..."
echo "About to host Angular application on nginx (Docker) ... "
sh local-docker-build.sh $containerId $imageId
echo "Angular application hosted on nginx (Docker) ... "
 