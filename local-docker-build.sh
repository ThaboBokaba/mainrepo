#!/bin/bash

containerIds="$(docker container ls -a --format='{{json .}}' | grep -o \"ID\":\".*\" | cut -f 1 -d ',' | cut -f 2 -d ':' | cut -f 2 -d \")"

while read -r containerId; do
    echo "About to stop and remove container with id [ $containerId ] ... "
    docker container stop $containerId && docker container rm $containerId
    echo "Container [ $containerId ] stopped... "
done <<< "$containerIds"

containerName=$1
imageName=$2

echo "Building Dockerfile on the current directory ... "
docker build -t $imageName .
echo "Docker image built on local repository... "

echo "About to create container with name [ $containerName ] with image [ $imageName ]"

docker run --name $containerName -p 80:80 -d $imageName

echo "Your deployment is completed. Please test out the application on port 80."