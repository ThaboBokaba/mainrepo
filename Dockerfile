FROM nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY angular-nginx-template/dist/angular-frontend-v7 /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d
COPY nginx-error-pages/ /usr/share/nginx/html
#VOLUME /data/www
# VOLUME /etc/nginx